using UnityEngine;

public class GemCollectible : EnemyCore {
	public override void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player") {
			Kill(true);
			GameManager.score += pointValue;
		}
	}
}
