using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[Flags]
public enum MapDirection {
	None = 0,
	Left = 1,
	Right = 2,
	Above = 4,
	Below = 8
}

public enum RoomType {
	Normal = 0,
	Checkpoint = 1,
	DeadEnd = 2,
}

public class MapRoom {
	public const int ROOM_WIDTH = 36;
	public const int ROOM_HEIGHT = 20;

	public RoomType roomType = RoomType.Normal;
	public MapRoom left = null;
	public MapRoom right = null;
	public MapRoom above = null;
	public MapRoom below = null;
	public MapDirection exits;

	public int leftY1 = ROOM_HEIGHT / 2 - 5;
	public int leftY2 = ROOM_HEIGHT / 2 + 5;
	public int leftX = (int)(ROOM_WIDTH * 0.25f);
	public int rightY1 = ROOM_HEIGHT / 2 - 5;
	public int rightY2 = ROOM_HEIGHT / 2 + 5;
	public int rightX = (int)(ROOM_WIDTH * 0.75f);
	public int topX1 = ROOM_WIDTH / 2 - 5;
	public int topX2 = ROOM_WIDTH / 2 + 5;
	public int topY = (int)(ROOM_HEIGHT * 0.25f);
	public int bottomX1 = ROOM_WIDTH / 2 - 5;
	public int bottomX2 = ROOM_WIDTH / 2 + 5;
	public int bottomY = (int)(ROOM_HEIGHT * 0.75f);

	public static int nextRoom = 1;
	public int roomID = 0;
	public int distanceToCheckpoint = -1;
	public int level;
	public MapDirection entrance;
	public int powered = 0;
	public int originX;
	public int originY;
	public Vector3 origin;

	public Material backgroundMaterial = null;

	public byte[,] tiles = new byte[ROOM_WIDTH, ROOM_HEIGHT];
	public Mesh mesh = null;

	public MapRoom(MapDirection entrance, int originX, int originY) {
		this.entrance = entrance;
		this.exits = entrance;
		this.roomID = nextRoom;
		nextRoom++;
		this.originX = originX;
		this.originY = originY;
		this.origin = new Vector3((float)originX * ROOM_WIDTH, (float)originY * -ROOM_HEIGHT, 0);

		topX1 = (int)(2 + UnityEngine.Random.value * MapRoom.ROOM_WIDTH * 0.6f);
		topX2 = (int)Mathf.Lerp(topX1 + 4, MapRoom.ROOM_WIDTH - 2, UnityEngine.Random.value);
		bottomX1 = (int)(2 + UnityEngine.Random.value * MapRoom.ROOM_WIDTH * 0.6f);
		bottomX2 = (int)Mathf.Lerp(bottomX1 + 4, MapRoom.ROOM_WIDTH - 2, UnityEngine.Random.value);
		leftX = (int)Mathf.Lerp(2, Mathf.Min(topX1, bottomX1), UnityEngine.Random.value);
		rightX = (int)Mathf.Lerp(Mathf.Max(topX2, bottomX2), MapRoom.ROOM_WIDTH - 2, UnityEngine.Random.value);
		leftY1 = (int)(2 + UnityEngine.Random.value * MapRoom.ROOM_HEIGHT * 0.4f);
		leftY2 = (int)Mathf.Lerp(leftY1 + 5, MapRoom.ROOM_HEIGHT - 2, UnityEngine.Random.value);
		rightY1 = (int)(2 + UnityEngine.Random.value * MapRoom.ROOM_HEIGHT * 0.4f);
		rightY2 = (int)Mathf.Lerp(rightY1 + 5, MapRoom.ROOM_HEIGHT - 2, UnityEngine.Random.value);
		topY = (int)Mathf.Lerp(2, Mathf.Min(leftY1, rightY1), UnityEngine.Random.value);
		bottomY = (int)Mathf.Lerp(Mathf.Max(leftY2, rightY2), MapRoom.ROOM_HEIGHT - 2, UnityEngine.Random.value);
	}

	public MapRoom GetExit(MapDirection direction) {
		if (direction == MapDirection.Above) return above;
		if (direction == MapDirection.Below) return below;
		if (direction == MapDirection.Left) return left;
		if (direction == MapDirection.Right) return right;
		return null;
	}
}

public class TileMap : LevelAwareBehaviour {
	private static float BACKGROUND_DEPTH = -0.1f;
	private static float TERRAIN_DEPTH = -0.3f;

	public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
	public List<EnemyCore> collectPrefabs = new List<EnemyCore>();
	public MapRoom mapRoot;
	[HideInInspector]
	public List<EnemyCore> enemies = new List<EnemyCore>();
	[HideInInspector]
	private List<MapRoom> allRooms = new List<MapRoom>();
	private MapRoom cameraRoom;
	private MapRoom dummyRoom;

	public Material material;
	public Texture2D backgroundTexture;
	public Shader checkpointShader;
	private Material backgroundMaterial;
	private Mesh backgroundMesh;

	private static float tileSizeH = 1f / 8f;
	private static float tileSizeV = 1f / 12f;
	private static Vector2 TileToUV(int x, int y) {
		return new Vector2(x * tileSizeH, (11 - y) * tileSizeV);
	}
	private static Vector2[] tileUV = {
		/* .0. 0*0 .0. */ TileToUV(4, 3),
		/* .0. 0*0 .1. */ TileToUV(3, 3),
		/* .0. 0*1 .0. */ TileToUV(0, 3),
		/* .0. 0*1 .1. */ TileToUV(0, 0),
		/* .0. 1*0 .0. */ TileToUV(2, 3),
		/* .0. 1*0 .1. */ TileToUV(2, 0),
		/* .0. 1*1 .0. */ TileToUV(1, 3),
		/* .0. 1*1 .1. */ TileToUV(1, 0),
		/* .1. 0*0 .0. */ TileToUV(3, 5),
		/* .1. 0*0 .1. */ TileToUV(3, 4),
		/* .1. 0*1 .0. */ TileToUV(0, 2),
		/* .1. 0*1 .1. */ TileToUV(0, 1),
		/* .1. 1*0 .0. */ TileToUV(2, 2),
		/* .1. 1*0 .1. */ TileToUV(2, 1),
		/* .1. 1*1 .0. */ TileToUV(1, 2),
		/* .1. 1*1 .1. */ TileToUV(1, 1),

		/* 0:0 :*: 0:0 */ TileToUV(1, 1),
		/* 0:0 :*: 0:1 */ TileToUV(1, 1),
		/* 0:0 :*: 1:0 */ TileToUV(1, 1),
		/* 0:0 :*: 1:1 */ TileToUV(4, 2),
		/* 0:1 :*: 0:0 */ TileToUV(1, 1),
		/* 0:1 :*: 0:1 */ TileToUV(5, 1),
		/* 0:1 :*: 1:0 */ TileToUV(1, 1),
		/* 0:1 :*: 1:1 */ TileToUV(5, 2),
		/* 1:0 :*: 0:0 */ TileToUV(1, 1),
		/* 1:0 :*: 0:1 */ TileToUV(1, 1),
		/* 1:0 :*: 1:0 */ TileToUV(3, 1),
		/* 1:0 :*: 1:1 */ TileToUV(3, 2),
		/* 1:1 :*: 0:0 */ TileToUV(4, 0),
		/* 1:1 :*: 0:1 */ TileToUV(5, 0),
		/* 1:1 :*: 1:0 */ TileToUV(3, 0),
		/* 1:1 :*: 1:1 */ TileToUV(1, 1),

		/* .0. 0*0 ... */ TileToUV(4, 4),
		/* .0. 0*1 ... */ TileToUV(2, 4),
		/* .0. 1*0 ... */ TileToUV(0, 4),
		/* .0. 1*1 ... */ TileToUV(1, 4),

		/* .0. 0*0 .0. */ TileToUV(0, 5),
		/* .0. 0*0 .1. */ TileToUV(0, 5),
		/* .0. 0*1 .0. */ TileToUV(0, 5),
		/* .0. 0*1 .1. */ TileToUV(0, 7),
		/* .0. 1*0 .0. */ TileToUV(0, 5),
		/* .0. 1*0 .1. */ TileToUV(1, 7),
		/* .0. 1*1 .0. */ TileToUV(0, 6),
		/* .0. 1*1 .1. */ TileToUV(0, 9),
		/* .1. 0*0 .0. */ TileToUV(0, 5),
		/* .1. 0*0 .1. */ TileToUV(1, 6),
		/* .1. 0*1 .0. */ TileToUV(0, 8),
		/* .1. 0*1 .1. */ TileToUV(0, 10),
		/* .1. 1*0 .0. */ TileToUV(1, 8),
		/* .1. 1*0 .1. */ TileToUV(1, 10),
		/* .1. 1*1 .0. */ TileToUV(1, 9),
		/* .1. 1*1 .1. */ TileToUV(1, 5),

		/* .0. 0*0 .0. */ TileToUV(2, 5),
		/* .0. 0*0 .1. */ TileToUV(2, 5),
		/* .0. 0*1 .0. */ TileToUV(2, 5),
		/* .0. 0*1 .1. */ TileToUV(2, 7),
		/* .0. 1*0 .0. */ TileToUV(2, 5),
		/* .0. 1*0 .1. */ TileToUV(3, 7),
		/* .0. 1*1 .0. */ TileToUV(2, 6),
		/* .0. 1*1 .1. */ TileToUV(2, 9),
		/* .1. 0*0 .0. */ TileToUV(2, 5),
		/* .1. 0*0 .1. */ TileToUV(3, 6),
		/* .1. 0*1 .0. */ TileToUV(2, 8),
		/* .1. 0*1 .1. */ TileToUV(2, 10),
		/* .1. 1*0 .0. */ TileToUV(3, 8),
		/* .1. 1*0 .1. */ TileToUV(3, 10),
		/* .1. 1*1 .0. */ TileToUV(3, 9),
		/* .1. 1*1 .1. */ TileToUV(4, 5),

		/* top-left  */ TileToUV(5, 3),
		/* top-mid   */ TileToUV(6, 3),
		/* top-right */ TileToUV(7, 3),
		/* mid-left  */ TileToUV(5, 4),
		/* mid-mid   */ TileToUV(6, 4),
		/* mid-right */ TileToUV(7, 4),
		/* bot-left  */ TileToUV(5, 5),
		/* bot-mid   */ TileToUV(6, 5),
		/* bot-right */ TileToUV(7, 5),
		/* power on  */ TileToUV(6, 0),
		/* power off */ TileToUV(6, 1)
	};

	public void GenerateTiles(MapRoom room) {
		for (int x = 0; x < MapRoom.ROOM_WIDTH; x++) {
			float top = room.topY;
			if (x < room.leftX || x > room.rightX) {
				top = MapRoom.ROOM_HEIGHT;
			} else if (x <= room.topX1) {
				top = Mathf.Lerp(room.leftY1, room.topY, (float)x / (float)room.topX1);
			} else if (x >= room.topX2) {
				top = Mathf.Lerp(room.topY, room.rightY1, (float)(x - room.topX2) / (float)(MapRoom.ROOM_WIDTH - room.topX2 - 1));
			}
			if ((int)top < 2 && (x <= room.topX1 || x >= room.topX2)) {
				top = 2;
			}

			float bottom = room.bottomY;
			if (x <= room.bottomX1) {
				bottom = Mathf.Lerp(room.leftY2, room.bottomY, (float)x / (float)room.bottomX1);
			} else if (x >= room.bottomX2) {
				bottom = Mathf.Lerp(room.bottomY, room.rightY2, (float)(x - room.bottomX2) / (float)(MapRoom.ROOM_WIDTH - room.bottomX2 - 1));
			}
			if ((int)bottom >= MapRoom.ROOM_HEIGHT - 2 && (x <= room.bottomX1 || x >= room.bottomX2)) {
				bottom = MapRoom.ROOM_HEIGHT - 2;
			} else if ((int)bottom < 2 && (x <= room.bottomX1 || x >= room.bottomX2)) {
				bottom = 2;
			}
			for (int y = 0; y < MapRoom.ROOM_HEIGHT; y++) {
				if (y < 3 && (room.above != null && room.above.tiles[x, MapRoom.ROOM_HEIGHT - 1] <= 64)) {
					room.tiles[x, y] = (byte)0;
				} else if (y < top || y >= bottom) {
					room.tiles[x, y] = (byte)65;
				} else {
					room.tiles[x, y] = (byte)0;
				}
			}
		}

		if (room.roomType == RoomType.Checkpoint) {
			AddCheckpoint(room);
			AddCheckpointWires(room);
		} else {
			AddWires(room);
		}

		PopulateMesh(room);
	}

	private void SetWire(MapRoom room, int x, int y, bool power = false) {
		if (room.tiles[x, y] > (byte)64) {
			room.tiles[x, y] = power ? (byte)68 : (byte)67;
		} else {
			room.tiles[x, y] = power ? (byte)2 : (byte)1;
		}
	}

	private void SetHWire(MapRoom room, int x1, int x2, int y, bool power = false) {
		if (x2 < x1) {
			int t = x1;
			x1 = x2;
			x2 = t;
		}
		for (int x = x1; x <= x2; x++) {
			SetWire(room, x, y, power);
		}
	}

	private void SetVWire(MapRoom room, int x, int y1, int y2, bool power = false) {
		if (y2 < y1) {
			int t = y1;
			y1 = y2;
			y2 = t;
		}
		for (int y = y1; y <= y2; y++) {
			SetWire(room, x, y, power);
		}
	}

	private void AddWires(MapRoom room)
	{
		bool power = room.powered == 2;
		int topX = (room.topX1 + room.topX2) / 2;
		int bottomX = (room.bottomX1 + room.bottomX2) / 2;
		int leftY = (room.leftY1 + room.leftY2) / 2;
		int rightY = (room.rightY1 + room.rightY2) / 2;

		int firstY = MapRoom.ROOM_HEIGHT - 1;
		int lastY = 0;
		if (room.leftX == 0) {
			firstY = leftY;
			lastY = leftY;
		}
		if (room.rightX == MapRoom.ROOM_WIDTH) {
			if (rightY < firstY) {
				firstY = rightY;
			}
			if (rightY > lastY) {
				lastY = rightY;
			}
		}
		if (room.topY == 0) {
			firstY = 0;
		} else {
			topX = bottomX;
		}
		if (room.bottomY == MapRoom.ROOM_HEIGHT) {
			lastY = MapRoom.ROOM_HEIGHT - 1;
		}
		if (lastY < firstY || lastY == 0) {
			lastY = MapRoom.ROOM_HEIGHT / 2;
		}
		for (int y = firstY; y <= lastY; y++) {
			SetWire(room, topX, y, power);
			if (y == leftY && room.leftX == 0) {
				SetHWire(room, 0, topX - 1, y, power);
			}
			if (y == rightY && room.rightX == MapRoom.ROOM_WIDTH) {
				SetHWire(room, topX, MapRoom.ROOM_WIDTH - 1, y, power);
			}
			if (y >= leftY && y >= rightY && topX != bottomX && room.bottomY == MapRoom.ROOM_HEIGHT) {
				SetHWire(room, topX, bottomX, y, power);
				topX = bottomX;
			}
		}
	}

	private void AddCheckpoint(MapRoom room)
	{
		int bgx = MapRoom.ROOM_WIDTH / 2 - 3;
		int bgy = MapRoom.ROOM_HEIGHT / 2 - 3;
		for (int x = bgx; x < bgx + 7; x++) {
			for(int y = bgy; y < bgy + 8; y++) {
				if (x == bgx) {
					room.tiles[x, y] = (byte)13;
				} else if (y == bgy) {
					room.tiles[x, y] = (byte)11;
				} else if (x == bgx + 6) {
					room.tiles[x, y] = (byte)15;
				} else if (y == bgy + 6) {
					room.tiles[x, y] = (byte)17;
				} else if (y == bgy + 7) {
					room.tiles[x, y] = (byte)67;
				} else {
					room.tiles[x, y] = (byte)14;
				}
			}
		}
		room.tiles[bgx,     bgy    ] = (byte)10;
		room.tiles[bgx + 6, bgy    ] = (byte)12;
		room.tiles[bgx,     bgy + 7] = (byte)16;
		room.tiles[bgx + 6, bgy + 7] = (byte)18;
		room.tiles[bgx + 3, bgy + 5] = (byte)20;
		room.tiles[bgx + 2, bgy + 6] = (byte)67;
		room.tiles[bgx + 3, bgy + 6] = (byte)67;
		room.tiles[bgx + 4, bgy + 6] = (byte)67;
	}

	void AddCheckpointWires(MapRoom room, bool power = false)
	{
		int topX = (room.topX1 + room.topX2) / 2;
		int bottomX = (room.bottomX1 + room.bottomX2) / 2;
		int leftY = (room.leftY1 + room.leftY2) / 2;
		int rightY = (room.rightY1 + room.rightY2) / 2;
		int bgx = MapRoom.ROOM_WIDTH / 2 - 3;
		int bgy = MapRoom.ROOM_HEIGHT / 2 - 3;

		if ((room.exits & MapDirection.Above) != MapDirection.None && (!power || room.entrance == MapDirection.Above)) {
			int adjY = bgy / 2;
			int adjX = (bgx <= topX && topX <= bgx + 6) ? topX : bgx + 3;
			SetVWire(room, topX, 0, adjY, power);
			SetHWire(room, topX, adjX, adjY, power);
			SetVWire(room, adjX, adjY, bgy - 1, power);
		}

		if ((room.exits & MapDirection.Below) != MapDirection.None && !power) {
			int adjY = bgy + 10;
			int adjX = (bgx <= bottomX && bottomX <= bgx + 6) ? bottomX : bgx + 3;
			SetVWire(room, bottomX, MapRoom.ROOM_HEIGHT - 1, adjY, power);
			SetHWire(room, bottomX, adjX, adjY, power);
			SetVWire(room, adjX, adjY, bgy + 8, power);
		}

		if ((room.exits & MapDirection.Left) != MapDirection.None && (!power || room.entrance == MapDirection.Left)) {
			int adjX = bgx / 2;
			int adjY = (bgy <= leftY && leftY <= bgy + 7) ? leftY : bgy + 3;
			SetHWire(room, 0, adjX, leftY, power);
			SetVWire(room, adjX, leftY, adjY, power);
			SetHWire(room, adjX, bgx - 1, adjY, power);
		}

		if ((room.exits & MapDirection.Right) != MapDirection.None && (!power || room.entrance == MapDirection.Right)) {
			int adjX = bgx + 7 + bgx / 2;
			int adjY = (bgy <= rightY && rightY <= bgy + 7) ? rightY : bgy + 3;
			SetHWire(room, bgx + 7, adjX, adjY, power);
			SetVWire(room, adjX, rightY, adjY, power);
			SetHWire(room, adjX, MapRoom.ROOM_WIDTH - 1, rightY, power);
		}
	}


	public void Awake()
	{
		backgroundMaterial = new Material(material);
		backgroundMaterial.mainTexture = backgroundTexture;
		backgroundMesh = new Mesh();
		backgroundMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(new Vector3(0, 0, 0));
		vertices.Add(new Vector3(MapRoom.ROOM_WIDTH, 0, 0));
		vertices.Add(new Vector3(MapRoom.ROOM_WIDTH, -MapRoom.ROOM_HEIGHT, 0));
		vertices.Add(new Vector3(0, -MapRoom.ROOM_HEIGHT, 0));
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));
		uv.Add(new Vector2(0, 1));
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);
		backgroundMesh.vertices = vertices.ToArray();
		backgroundMesh.uv = uv.ToArray();
		backgroundMesh.triangles = triangles.ToArray();

		NewMapRoot();

		dummyRoom = new MapRoom(MapDirection.None, 0, 0);
		for (int y = 0; y < MapRoom.ROOM_HEIGHT; y++) {
			for (int x = 0; x < MapRoom.ROOM_WIDTH; x++) {
				dummyRoom.tiles[x, y] = (byte)65;
			}
		}
		PopulateMesh(dummyRoom);
	}

	private void NewMapRoot()
	{
		mapRoot = new MapRoom(MapDirection.None, 0, 0);
		mapRoot.powered = 0;
		mapRoot.exits = MapDirection.Right;
		allRooms = new List<MapRoom>{ mapRoot };
		mapRoot.distanceToCheckpoint = 4;
		mapRoot.level = 1;
		mapRoot.rightX = MapRoom.ROOM_WIDTH;
		mapRoot.bottomY = MapRoom.ROOM_HEIGHT / 2 + 2;
		cameraRoom = mapRoot;
		GenerateTiles(mapRoot);
		BuildAttachedRooms(cameraRoom);
		UpdateBackground(mapRoot);
	}

	public override void OnGameStart() {
		foreach (EnemyCore enemy in enemies) {
			Destroy(enemy.gameObject);
		}
		enemies.Clear();
	}

	public override void OnPlayerSpawn() {
		cameraRoom = GameObject.FindWithTag("Player").GetComponent<PlayerController>().currentRoom;
		UpdateObjectVisibility();
	}

	public void NewLevel() {
		NewMapRoot();
		cameraRoom = mapRoot;
	}

	MapDirection RandomExits(MapDirection back, bool deadEnd) {
		MapDirection result = back;
		float rng = UnityEngine.Random.value;
		if (deadEnd) {
			if (rng < 0.5) {
				result |= (back == MapDirection.Left) ? MapDirection.Right : MapDirection.Left;
			} else if (rng < 0.8) {
				result |= MapDirection.Below;
			}
		} else if (back == MapDirection.Above) {
			if (rng < 0.7) {
				result |= MapDirection.Left;
			}
			if (rng > 0.3) {
				result |= MapDirection.Right;
			}
		} else {
			if (rng > 0.3) {
				result |= MapDirection.Below;
			}
			if (rng < 0.7) {
				result |= (back == MapDirection.Right) ? MapDirection.Left : MapDirection.Right;
			}
		}
		return result;
	}

	void UpdateRelatedRoom(MapRoom oldRoom, MapRoom newRoom) {
		newRoom.distanceToCheckpoint = oldRoom.distanceToCheckpoint - 1;
		newRoom.level = oldRoom.level;
		newRoom.powered = oldRoom.powered;
		if (newRoom.distanceToCheckpoint == 0) {
			newRoom.distanceToCheckpoint = oldRoom.level + 4;
			newRoom.roomType = RoomType.Checkpoint;
		} else {
			newRoom.distanceToCheckpoint = oldRoom.distanceToCheckpoint - 1;
		}
		UpdateBackground(newRoom);
	}

	void BuildAttachedRooms(MapRoom room) {
		if (room == null) {
			return;
		}
		List<MapRoom> newRooms = new List<MapRoom>();

		if ((room.exits & MapDirection.Left) != MapDirection.None && room.left == null) {
			MapRoom left = new MapRoom(MapDirection.Right, room.originX - 1, room.originY);
			newRooms.Add(left);
			room.left = left;
			left.right = room;
			UpdateRelatedRoom(room, left);
			left.rightY1 = room.leftY1;
			left.rightY2 = room.leftY2;
		}

		if ((room.exits & MapDirection.Right) != MapDirection.None && room.right == null) {
			MapRoom right = new MapRoom(MapDirection.Left, room.originX + 1, room.originY);
			newRooms.Add(right);
			room.right = right;
			right.left = room;
			UpdateRelatedRoom(room, right);
			right.leftY1 = room.rightY1;
			right.leftY2 = room.rightY2;
		}

		if ((room.exits & MapDirection.Below) != MapDirection.None && room.below == null) {
			MapRoom below = new MapRoom(MapDirection.Above, room.originX, room.originY + 1);
			newRooms.Add(below);
			room.below = below;
			below.above = room;
			UpdateRelatedRoom(room, below);
			below.topX1 = room.bottomX1;
			below.topX2 = room.bottomX2;
		}

		if (newRooms.Count > 0) {
			MapRoom correctRoom = newRooms[(int)(newRooms.Count * UnityEngine.Random.value)];
			for (int i = 0; i < newRooms.Count; i++) {
				MapRoom newRoom = newRooms[i];
				allRooms.Add(newRoom);
				MapDirection back =
					(newRoom.left == room) ? MapDirection.Left :
					(newRoom.right == room) ? MapDirection.Right :
					MapDirection.Above;
				if (newRoom.distanceToCheckpoint < -3) {
					newRoom.exits = back;
					newRoom.roomType = RoomType.DeadEnd;
				} else if (newRoom != correctRoom || newRoom.distanceToCheckpoint < 0) {
					if (newRoom.distanceToCheckpoint >= 0) {
						newRoom.distanceToCheckpoint = -1;
					}
					newRoom.roomType = RoomType.DeadEnd;
					newRoom.exits = RandomExits(back, true);
				} else {
					newRoom.exits = RandomExits(back, false);
				}
				if ((newRoom.exits & MapDirection.Left) != MapDirection.None) {
					newRoom.leftX = 0;
				} else if (newRoom.leftX < 2) {
					newRoom.leftX = 2;
				}
				if ((newRoom.exits & MapDirection.Right) != MapDirection.None) {
					newRoom.rightX = MapRoom.ROOM_WIDTH;
				} else if (newRoom.rightX > MapRoom.ROOM_WIDTH - 2) {
					newRoom.leftX = MapRoom.ROOM_WIDTH - 2;
				}
				if ((newRoom.exits & MapDirection.Above) != MapDirection.None) {
					newRoom.topY = 0;
				} else if (newRoom.topY < 2) {
					newRoom.topY = 2;
				}
				if ((newRoom.exits & MapDirection.Below) != MapDirection.None) {
					newRoom.bottomY = MapRoom.ROOM_HEIGHT;
				} else if (newRoom.bottomY > MapRoom.ROOM_HEIGHT - 2) {
					newRoom.bottomY = MapRoom.ROOM_HEIGHT - 2;
				}
				GenerateTiles(newRoom);
				GenerateEnemies(newRoom);

			}
			if (correctRoom.roomType == RoomType.Checkpoint) {
				correctRoom.level++;
				correctRoom.powered = 0;
				UpdateBackground(correctRoom);
			}
		}
	}

	void GenerateEnemies(MapRoom room) {
		int numEnemies = (int)(UnityEngine.Random.value * room.level);
		for (int i = 0; i < numEnemies; i++) {
			EnemyCore newEnemy = Instantiate(enemyPrefabs[0]);
			newEnemy.currentRoom = room;
			int tx, ty;
			do {
				tx = (int)(UnityEngine.Random.value * MapRoom.ROOM_WIDTH);
				ty = (int)(UnityEngine.Random.value * MapRoom.ROOM_HEIGHT);
			} while (room.tiles[tx, ty] > 64);
			newEnemy.origin = new Vector2(MapRoom.ROOM_WIDTH * room.originX + tx, MapRoom.ROOM_HEIGHT * room.originY + ty + 1);
			enemies.Add(newEnemy);
		}
		bool hasCollectible = UnityEngine.Random.value > 0.6f;
		if (hasCollectible) {
			EnemyCore newEnemy = Instantiate(collectPrefabs[(int)(collectPrefabs.Count * UnityEngine.Random.value)]);
			newEnemy.currentRoom = room;
			int tx, ty;
			do {
				tx = (int)(UnityEngine.Random.value * MapRoom.ROOM_WIDTH);
				ty = (int)(UnityEngine.Random.value * MapRoom.ROOM_HEIGHT);
			} while (room.tiles[tx, ty] > 64);
			newEnemy.origin = new Vector2(MapRoom.ROOM_WIDTH * room.originX + tx, MapRoom.ROOM_HEIGHT * room.originY + ty + 1);
			enemies.Add(newEnemy);
		}
	}

	public byte TileTypeAt(float x, float y, MapRoom room)
	{
		if (room == null) {
			return (byte)65;
		}
		float lx = x - room.origin.x;
		float ly = y + room.origin.y;
		if (lx < 0) {
			if ((room.exits & MapDirection.Left) == MapDirection.None) {
				return 65;
			}
			return TileTypeAt(x, y, room.left);
		}
		if (lx >= MapRoom.ROOM_WIDTH) {
			if ((room.exits & MapDirection.Right) == MapDirection.None) {
				return 65;
			}
			return TileTypeAt(x, y, room.right);
		}
		if (ly < 0) {
			if ((room.exits & MapDirection.Above) == MapDirection.None) {
				return 65;
			}
			return TileTypeAt(x, y, room.above);
		}
		if (ly >= MapRoom.ROOM_HEIGHT) {
			if ((room.exits & MapDirection.Below) == MapDirection.None) {
				return 65;
			}
			return TileTypeAt(x, y, room.below);
		}
		return room.tiles[(int)lx, (int)ly];
	}

	public bool IsSolid(float x, float y, MapRoom room)
	{
		byte tileType = TileTypeAt(x, y, room);
		return tileType > 64;
	}

	public bool IsWire(float x, float y, MapRoom room)
	{
		byte tileType = TileTypeAt(x, y, room);
		return tileType == 1 || tileType == 2 || tileType == 67 || tileType == 68 || (tileType >= 10 && tileType <= 20);
	}

	public void PopulateMesh(MapRoom room)
	{
		if (room.mesh == null) {
			room.mesh = new Mesh();
		}
		Mesh mesh = room.mesh;
		byte[,] data = room.tiles;
		mesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleBase = 0;
		for (int y = 0; y < MapRoom.ROOM_HEIGHT; y++) {
			int wy = y + room.originY * MapRoom.ROOM_HEIGHT;
			for (int x = 0; x < MapRoom.ROOM_WIDTH; x++) {
				if (data[x, y] == 0) continue;
				int wx = x + room.originX * MapRoom.ROOM_WIDTH;
				vertices.Add(new Vector3(x, -y, 0));
				vertices.Add(new Vector3(x + 1, -y, 0));
				vertices.Add(new Vector3(x + 1, -y - 1, 0));
				vertices.Add(new Vector3(x, -y - 1, 0));
				int bits = 0;
				bool hasTop = false;
				if (data[x, y] >= 10 && data[x, y] <= 20) {
					bits = data[x, y] + (byte)58;
				} else if (data[x, y] == 1) {
					if (IsWire(wx, wy-1, room) || y < 2) bits |= 8;
					if (IsWire(wx-1, wy, room) || x < 2) bits |= 4;
					if (IsWire(wx+1, wy, room) || x > MapRoom.ROOM_WIDTH - 3) bits |= 2;
					if (IsWire(wx, wy+1, room) || y > MapRoom.ROOM_HEIGHT - 3) bits |= 1;
					bits += 36;
				} else if (data[x, y] == 2) {
					bits = 16;
					if (IsWire(wx, wy-1, room) || y < 2) bits |= 8;
					if (IsWire(wx-1, wy, room) || x < 2) bits |= 4;
					if (IsWire(wx+1, wy, room) || x > MapRoom.ROOM_WIDTH - 3) bits |= 2;
					if (IsWire(wx, wy+1, room) || y > MapRoom.ROOM_HEIGHT - 3) bits |= 1;
					bits += 36;
				} else {
					if (IsSolid(wx, wy-1, room)) bits |= 8;
					if (IsSolid(wx-1, wy, room)) bits |= 4;
					if (IsSolid(wx+1, wy, room)) bits |= 2;
					if (IsSolid(wx, wy+1, room)) bits |= 1;
					hasTop = (bits & 8) == 0;
					if (bits == 15) {
						bits = 16;
						if (IsSolid(wx-1, wy-1, room)) bits |= 8;
						if (IsSolid(wx+1, wy-1, room)) bits |= 4;
						if (IsSolid(wx-1, wy+1, room)) bits |= 2;
						if (IsSolid(wx+1, wy+1, room)) bits |= 1;
					}
				}
				Vector2 uvPos = tileUV[bits];
				uv.Add(new Vector2(uvPos.x,            uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y));
				uv.Add(new Vector2(uvPos.x,            uvPos.y));
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+1);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase+3);
				triangleBase += 4;
				if (hasTop) {
					int topBits = 32 | (bits & 6 >> 1);
					uvPos = tileUV[topBits];
					vertices.Add(new Vector3(x, -y + 1, 0));
					vertices.Add(new Vector3(x + 1, -y + 1, 0));
					vertices.Add(new Vector3(x + 1, -y, 0));
					vertices.Add(new Vector3(x, -y, 0));
					uv.Add(new Vector2(uvPos.x,            uvPos.y + tileSizeV));
					uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y + tileSizeV));
					uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y));
					uv.Add(new Vector2(uvPos.x,            uvPos.y));
					triangles.Add(triangleBase);
					triangles.Add(triangleBase+1);
					triangles.Add(triangleBase+2);
					triangles.Add(triangleBase);
					triangles.Add(triangleBase+2);
					triangles.Add(triangleBase+3);
					triangleBase += 4;
				}
			}
		}
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = triangles.ToArray();
	}

	private void DrawRoom(MapRoom room, int offsetX, int offsetY) {
		Vector3 position = cameraRoom.origin + new Vector3(offsetX * MapRoom.ROOM_WIDTH, offsetY * MapRoom.ROOM_HEIGHT, BACKGROUND_DEPTH);
		position.z = position.z + (Mathf.Abs(offsetX) + Mathf.Abs(offsetY)) * 0.01f;
		Material bg = backgroundMaterial;
		if (room != null) {
			if (room.backgroundMaterial == null) {
				room.backgroundMaterial = backgroundMaterial;
			}
			bg = room.backgroundMaterial;
		}
		Graphics.DrawMesh(backgroundMesh, position, Quaternion.identity, bg, 0, null, 0, null, false, false, false);
		position.z = TERRAIN_DEPTH;
		Graphics.DrawMesh(room == null ? dummyRoom.mesh : room.mesh, position, Quaternion.identity, material, 0, null, 0, null, false, false, false);
	}

	public void Update() {
		DrawRoom(cameraRoom.above != null ? cameraRoom.above.left : null, -1, 1);
		DrawRoom(cameraRoom.above != null ? cameraRoom.above.right : null, 1, 1);
		DrawRoom(cameraRoom.below != null ? cameraRoom.below.left : null, -1, -1);
		DrawRoom(cameraRoom.below != null ? cameraRoom.below.right : null, 1, -1);
		DrawRoom(cameraRoom.left, -1, 0);
		DrawRoom(cameraRoom.right, 1, 0);
		DrawRoom(cameraRoom.above, 0, 1);
		DrawRoom(cameraRoom.below, 0, -1);
		DrawRoom(cameraRoom, 0, 0);
	}

	public void LateUpdate() {
		float cameraX = Camera.main.transform.position.x - cameraRoom.origin.x;
		float cameraY = -(Camera.main.transform.position.y - cameraRoom.origin.y);
		MapRoom lastCameraRoom = cameraRoom;
		if (cameraX < 0 && cameraRoom.left != null) {
			cameraRoom = cameraRoom.left;
		} else if (cameraX > MapRoom.ROOM_WIDTH && cameraRoom.right != null) {
			cameraRoom = cameraRoom.right;
		} else if (cameraY < 0 && cameraRoom.above != null) {
			cameraRoom = cameraRoom.above;
		} else if (cameraY > MapRoom.ROOM_HEIGHT && cameraRoom.below != null) {
			cameraRoom = cameraRoom.below;
		}
		if (cameraRoom != lastCameraRoom) {
			Debug.Log(String.Format("{2} {0}: {1}", cameraRoom.roomType, cameraRoom.exits, cameraRoom.roomID));
			BuildAttachedRooms(cameraRoom);
			BuildAttachedRooms(cameraRoom.above);
			BuildAttachedRooms(cameraRoom.below);
			UpdateObjectVisibility();
		}
	}

	private void PowerRoomRecursive(MapRoom room, MapRoom source) {
		if (room == null || room.level >= source.level || room.powered == 2) {
			return;
		}
		room.powered = 2;
		for(int x = 0; x < MapRoom.ROOM_WIDTH; x++) {
			for(int y = 0; y < MapRoom.ROOM_HEIGHT; y++) {
				if (room.tiles[x, y] == 1) {
					room.tiles[x, y] = 2;
				} else if (room.tiles[x, y] == 20) {
					room.tiles[x, y] = 19;
				}
			}
		}
		PopulateMesh(room);
		PowerRoomRecursive(room.left, source);
		PowerRoomRecursive(room.right, source);
		PowerRoomRecursive(room.above, source);
		PowerRoomRecursive(room.below, source);
		UpdateBackground(room);
	}

	public void PowerRoom(MapRoom room) {
		room.powered = 1;
		AddCheckpointWires(room, true);
		room.tiles[MapRoom.ROOM_WIDTH / 2, MapRoom.ROOM_HEIGHT / 2 + 2] = (byte)19;
		PopulateMesh(room);
		PowerRoomRecursive(room.GetExit(room.entrance), room);
		UpdateBackground(room);
	}

	public void UpdateObjectVisibility() {
		// Heads up: FindObjectsOfTypeAll is slow. This is only acceptable because we're only doing it on room transition.
		foreach (UnityEngine.Object obj in Resources.FindObjectsOfTypeAll(typeof(CharacterCore))) {
			CharacterCore ch = (CharacterCore)obj;
			ch.gameObject.SetActive(
				ch.currentRoom == cameraRoom ||
				ch.currentRoom == cameraRoom.above ||
				ch.currentRoom == cameraRoom.below ||
				ch.currentRoom == cameraRoom.left ||
				ch.currentRoom == cameraRoom.right
			);
		}
	}

	private Vector3 GetBackgroundShifts(MapRoom room) {
		float hueShift = (room.level - 1) / 360f * 103f;
		float satShift = (room.powered > 0) ? -.3f : .2f;
		float valShift = (room.powered > 0) ?
			(room.level - 1) * -0.01f + 0.1f :
			(room.level - 1) * -0.02f + 0.05f;
		return new Vector3(hueShift, satShift, valShift);
	}

	private void UpdateBackground(MapRoom room) {
		Material bg = new Material(backgroundMaterial);
		Vector3 shifts = GetBackgroundShifts(room);
		if (room.roomType == RoomType.Checkpoint) {
			bg.shader = checkpointShader;
			Vector3 shifts1 = GetBackgroundShifts(room.above == null ? room : room.above);
			Vector3 shifts2 = GetBackgroundShifts(room.right == null ? room : room.right);
			Vector3 shifts3 = GetBackgroundShifts(room.below == null ? room : room.below);
			Vector3 shifts4 = GetBackgroundShifts(room.left == null ? room : room.left);
			bg.SetVector("_HueShifts", new Vector4(shifts1.x, shifts2.x, shifts3.x, shifts4.x));
			bg.SetVector("_SaturationShifts", new Vector4(shifts1.y, shifts2.y, shifts3.y, shifts4.y));
			bg.SetVector("_ValueShifts", new Vector4(shifts1.z, shifts2.z, shifts3.z, shifts4.z));
		} else {
			bg.SetFloat("_HueShift", shifts.x);
			bg.SetFloat("_SaturationShift", shifts.y);
			bg.SetFloat("_ValueShift", shifts.z);
		}
		room.backgroundMaterial = bg;
	}
}
